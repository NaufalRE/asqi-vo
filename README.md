# WorkAdventure Maps

Currently this repo contains only file of the starter kit found [here](https://github.com/thecodingmachine/workadventure-map-starter-kit)

To understand how to use this starter kit, follow the tutorial at [https://workadventu.re/map-building](https://workadventu.re/map-building).
